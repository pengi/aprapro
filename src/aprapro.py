#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2009  Max Sikström - SA6BBC
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
from pyax import PyAx
from pyax.kiss import Kiss
from pyax.igate import *
from pyax.packet import *
from pyax.aprs import *
from pyax.database import PktDb

import config

class Router( PyAx ):
    addr = config.callsign
    timer = config.beacontime

#    inboxpath = "pkgs"

    def __init__( self ):
        self.do_init()

        self.tnc = Kiss( '/dev/soundmodem0' )
        self.reg( self.tnc )

        self.igate = None
        self.igate = Igate( 'ahubswe.net', self.addr.name + '-' + str( self.addr.ssid ), config.igate_pass, 14579 )
        self.reg( self.igate )

        self.db = PktDb( 'database.sqlite' )


    def dotimeout( self ):
        print "============================================================"
        print "Beacon:"

        pkt = Packet()
        pkt.src = self.addr
        pkt.dst = Addr( 'APZ123', 0, 0 )
        pkt.path = [Addr( 'WIDE2', 2, 0 )]
        pkt.info = AprsInfo( None, pkt )
        pkt.info.params['position'] = (config.crd_lat, config.crd_lon)
        pkt.info.params['symbol']   = config.symbol
        pkt.info.info               = config.comment

        print pkt
        self.tnc.transmit( pkt )

    def route( self, pkt, mod ):
        aprsify( pkt )
        if mod == self.tnc: ## Path from RF
            print "----------------------------------- RF"
            print pkt
            self.igate.transmit( pkt ) # Doesn't transmit yet... just debugging.

            if not self.db.avalible_since( pkt, '-30 seconds' ):
                self.db.add_packet( pkt )

            	rf_out_pkt = pkt.clone()
                if self.inchop( rf_out_pkt ):
                    self.tnc.transmit( rf_out_pkt )
                    print "Sending"
                    print rf_out_pkt
                else:
                    print "Not for routing"
            else:
                print "Duplicate"

#        if mod == self.igate: ## Path from I-gate
#            print "----------------------------------- Igate"
#            rfpkt = i_to_rf(    pkt,
#                                self.addr,
#                                config.i_to_rf_dst,
#                                [p.clone for p in config.i_to_rf_path]
#                                )
#            print pkt
#            print rfpkt



    def inchop( self, pkt ):
        id, hop = pkt.getActiveAddr()
        if hop != None:
            if self.addr.match( hop ):
                hop.setcflag()
                pkt.replacePath( id, hop )
                return True
            if hop.name[:5] == 'WIDE1':
                if hop.name[4:].isdigit():
                    hop.ssid -= 1
                    if hop.ssid <= 0:
                        hop.ssid = 0
                        hop.setcflag()
                    pkt.replacePath( id, [self.addr, hop] )
                    return True
        return False

    def handleexception( self, e ):
        print e
        f = open( 'error.log', 'a' )
        print >>f, e
        f.close()



if __name__=='__main__':
    r = Router()
    r.run()
