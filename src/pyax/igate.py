#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2009  Max Sikström - SA6BBC
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from socket import *
from thirdparty import ThirdPartyTag
import packet

class Igate:
    def __init__( self, addr, user, passcode = -1, port = 14580,type = AF_INET, proto = SOCK_STREAM ):
        self.sock = socket( type, proto )
        self.sock.connect( (addr, port) )
        self.sock.send( 'user %s pass %d\n' % (user, passcode) )
        self.sock.setblocking(0)
        self.buffer = ""
        self.user = user

    def getFds( self ):
        return [(self.sock.fileno(), -1)]

    def recieve( self ):
        self.buffer += self.sock.recv( 1024 )
        lines = self.buffer.splitlines(True)
        self.buffer = lines[-1]
        lines = lines[:-1]

        for l in lines:
            l = l.rstrip('\n\r')
            if len( l ) > 0 and l[0] != '#':
                addrstr, info = l.split(':',1)
                addrs = ThirdPartyTag( addrstr )

                pkt = packet.Packet()
                pkt.src = addrs.src
                pkt.dst = addrs.dst
                pkt.path = addrs.path
                pkt.ctrl = 3
                pkt.pid = 240
                pkt.info = packet.PacketInfo( info, pkt )
                yield pkt

    def transmit( self, pkt, mode='qAR' ):
        outp = pkt.src.plain_encode() + '>' + pkt.dst.plain_encode()
        for node in pkt.path:
            outp += "," + node.plain_encode()
        outp += "," + mode + "," + self.user
        outp += ":"
        outp += pkt.info.ax25_encode()
        dir( pkt )
        print "Sending: "+outp

def i_to_rf( pkt, src, dst, path=[] ):
    thirdparty = ThirdPartyTag()
    thirdparty.src = pkt.src
    thirdparty.dst = pkt.dst
    thirdparty.path = []
    keep = True
    for node in pkt.path:
        if keep:
            if node.name[0] == 'q':
                keep = False
            else:
                thirdparty.path.append( node )

    infostr = '}' + thirdparty.ax25_encode()
    infostr += ':' + pkt.info.ax25_encode()

    out = packet.Packet()
    out.src = src.clone()
    out.dst = dst.clone()
    out.path = [ node.clone() for node in path ]
    out.ctrl = pkt.ctrl # FIXME
    out.pid = pkt.pid # FIXME
    out.info = packet.PacketInfo( infostr, out )

    return out
