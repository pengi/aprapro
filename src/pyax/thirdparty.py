#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2009  Max Sikström - SA6BBC
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from packet import Addr

class ThirdPartyTag:
    src = None
    dst = None
    path = None
    def __init__( self, tag=None ):
        self.tag = tag
        if tag != None:
            src, rest = tag.split('>',1)
            path = rest.split(',')
            dst = path[0]
            path = path[1:]

            self.src = decode_straddr( src )
            self.dst = decode_straddr( dst )
            self.path = [decode_straddr( adr ) for adr in path]

    def __str__( self ):
        s = "{src=" + str( self.src ) + " dst=" + str( self.dst ) + " path"
        delim = "="
        for adr in self.path:
            s += delim + str(adr)
            delim = ","
        s+="}"
        return s

    def __repr__( self ):
        return "ThirdPartyTag(" + repr(self.tag) + ")"

    def ax25_encode( self ):
        out = self.src.plain_encode() + '>' + self.dst.plain_encode()
        for node in self.path:
            out += ',' + node.plain_encode()
        return out

def decode_straddr( adrstr ):
    flags = 3
    if adrstr[-1] == '*':
        flags |= 4
        adrstr = adrstr[:-1]

    if adrstr.find("-") >= 0:
        name, ssidstr = adrstr.split("-")
        if ssidstr.isdigit():
            ssid = int( ssidstr )
        else:
            ssid = 100 + int( ssidstr, 36 )
    else:
        name = adrstr
        ssid = 0

    return Addr( name, ssid, flags )
