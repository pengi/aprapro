#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2009  Max Sikström - SA6BBC
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import fcntl
import termios
from packet import ax25_decode

class Kiss:
    def __init__( self, fname, fd=None ):
        self.buffer = ""
        if fname != None:
            fd = os.open( fname, os.O_RDWR | os.O_NOCTTY | os.O_DSYNC )
        self.fd = fd

        fcntl.fcntl( self.fd, fcntl.F_SETFL, fcntl.fcntl( self.fd, fcntl.F_GETFL ) | os.O_NONBLOCK | os.O_DSYNC )

        while os.read( self.fd, 1024 ):
            print "Flushing input"


    def getFds( self ):
        return [(self.fd, -1)]

    def recieve( self ):
        self.buffer += os.read( self.fd, 1024 )
        for frame in self.__rxframe():
            head = ord(frame[0])
            if head == 0:
                packet = ax25_decode( frame[1:] )
                yield packet

    def transmitraw( self, frames ):
        for frame in frames:
            os.write( self.fd, self.__txframe( frame ) )
        os.write( self.fd, "\xc0" )
        termios.tcdrain( self.fd )

    def transmit( self, pkt ):
        self.transmitraw( ["\x00" + pkt.ax25_encode()] )

    def __rxframe( self ):
        while True:
            idx = self.buffer.find( "\xc0" )
            if idx < 0:
                return
            if idx > 0:
                frame = self.buffer[:idx]
                frame = frame.replace( "\xdb\xdc", "\xc0" )
                frame = frame.replace( "\xdb\xdd", "\xdb" )
                yield frame
            self.buffer = self.buffer[(idx+1):]

    def __txframe( self, frame ):
        outbuf = frame
        outbuf = outbuf.replace( "\xdb", "\xdb\xdd" )
        outbuf = outbuf.replace( "\xc0", "\xdb\xdc" )
        return "\xc0" + outbuf
