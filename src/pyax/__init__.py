#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2009  Max Sikström - SA6BBC
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import select
import time

__all__ = ['kiss','packet','aprs']

class PyAx:
    fds = {}
    running = True

    timer = 1.0

    nexttimer = None

    def do_init( self ):
        self.poller = select.poll()

    def __init__( self ):
        self.do_init()

    def reg( self, module ):
        for fd, eventmask in module.getFds():
            self.fds[fd] = module
            if eventmask < 0:
                eventmask = select.POLLIN | select.POLLPRI | select.POLLERR
            self.poller.register( fd, eventmask )

    def unreg( self, module ):
        for fd, eventmask in module.getFds():
            self.triggers.pop(fd, None)
            self.poller.unregister( fd )

    def recieve( self ):
        curt = time.time()
        if self.timer != None and self.nexttimer != None:
            timeout = int((self.nexttimer-curt)*1000.0)
        else:
            timeout = 1000 # Just to prevent freezing

        events = self.poller.poll( timeout )

        for fd, event in events:
            packets = self.fds[fd].recieve()
            for pkt in packets:
                yield (self.fds[fd], pkt)

        if self.timer != None:
            curt = time.time()
            if self.nexttimer == None or curt >= self.nexttimer:
                self.nexttimer = curt+self.timer
                yield (None, 0)

    def run( self ):
        while self.running:
            try:
                for mod, pkt in self.recieve():
                    if mod == None: # timeout
                        self.dotimeout()
                    elif pkt != None: # mod = module
                        self.route( pkt, mod )
            except Exception, e:
                self.handleexception( e )

    def handleexception( e ):
        print e

    def route( self, pkt, mod ):
        pass

    def dotimeout( self ):
        pass
