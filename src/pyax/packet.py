#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2009  Max Sikström - SA6BBC
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Addr:
    name = 'NULL'
    ssid = 0
    flags = 3
    def __init__( self, name, ssid, flags ):
        self.name = name
        self.ssid = ssid
        self.flags = flags

    def __str__( self ):
        return self.plain_encode()

    def __repr__( self ):
        return 'Addr("%s",%u,%u)' % (self.name, self.ssid, self.flags)

    def clone( self ):
        return Addr( self.name, self.ssid, self.flags )

    def cflag( self ):
        return (self.flags & 4) == 4

    def setcflag( self, value = True ):
        self.flags = (self.flags & 3)
        if value:
            self.flags |= 4

    def ax25_encode( self ):
        outbuf = (self.name + "      ")[0:6] + chr( self.flags << 4 | self.ssid )
        outstr = ""
        for c in outbuf:
            outstr += chr( ord( c ) << 1 )
        return outstr

    def plain_encode( self ):
        out = self.name
        if self.ssid>0:
            out += "-" + str( self.ssid )
        if self.flags & 4:
            out += "*"
        return out

    def match( self, pkt ):
        return pkt.name == self.name and pkt.ssid == self.ssid

class PacketInfo:
    rawinfo = ""
    def __init__( self, rawinfo, pkt=None ):
        self.rawinfo = rawinfo

    def __str__( self ):
        return "info " + repr( self.rawinfo )

    def ax25_encode( self ):
        return self.rawinfo

    def clone( self ):
        return PacketInfo( self.rawinfo )

class Packet:
    dst = Addr('',0,0)
    src = Addr('',0,0)
    path = []
    ctrl = 3
    pid = 240
    def __init__( self ):
        self.info = PacketInfo('',self)

    def clone( self ):
        pkt = Packet()
        pkt.info = self.info.clone()
        pkt.src = self.src.clone()
        pkt.dst = self.dst.clone()
        pkt.path = [p.clone() for p in self.path]
        pkt.ctrl = self.ctrl
        pkt.pid = self.pid
        return pkt

    def __repr__( self ):
        out = "<packet"
        out += " fm: " + str( self.src )
        out += " to: " + str( self.dst )
        if len( self.path ) > 0:
            out += " via:"
            for addr in self.path:
                out += " " + str( addr )
        out += " ctrl: " + str( self.ctrl )
        out += " pid: " + str( self.pid )
        out += " msg: " + str( self.info )
        out += ">"
        return out

    def getActiveAddr( self ):
        for id, pkt in enumerate( self.path ):
            if not pkt.cflag():
                return id, pkt.clone()
        return 0, None

    def replacePath( self, id, elems ):
        for i, elem in enumerate( elems ):
            elems[i] = elem.clone()
        self.path[id:(id+1)] = elems

    def ax25_encode( self ):
        outbuf = ""
        outbuf += self.dst.ax25_encode()
        outbuf += self.src.ax25_encode()
        for addr in self.path:
            outbuf += addr.ax25_encode()

        return    outbuf[:-1] \
                + chr(ord(outbuf[-1]) | 1) \
                + chr( self.ctrl ) \
                + chr( self.pid ) \
                + self.info.ax25_encode()

    def containpath( self, name, ssid = None ):
        if self.src.name == name and (ssid == None or self.src.ssid == ssid):
            return True
        if self.dst.name == name and (ssid == None or self.dst.ssid == ssid):
            return True
        for p in self.path:
            if p.name == name and (ssid == None or p.ssid == ssid):
                return True
        return False


def ax25_decode( frame ):

    pkt = Packet()

    left = len( frame )
    ptr = 0


    ##### Addresses and path

    in_addr = True
    addr = ""

    while in_addr:
        if left == 0:
            return None

        byte = ord( frame[ptr] )
        ptr += 1
        left -= 1

        addr += chr( byte>>1 )
        if byte & 1 == 1:
            in_addr = False


    pkt.path = []
    while len(addr)>=7:
        flags = ord(addr[6])
        pkt.path.append( Addr( addr[0:6].rstrip(), flags&0x0F, flags>>4 ) )
        addr = addr[7:]

    if len( addr ) != 0:
        return None

    if len( pkt.path ) < 2:
        return None

    pkt.dst  = pkt.path[0]
    pkt.src  = pkt.path[1]
    pkt.path = pkt.path[2:]




    ##### Control bytes

    if left < 1:
        return None
    pkt.ctrl = ord( frame[ptr] )
    ptr += 1
    left += 1

    # FIXME: different types and ctrl-fields


    if left < 1:
        return None
    pkt.pid = ord( frame[ptr] )
    ptr += 1
    left += 1

    pkt.info = PacketInfo( frame[ptr:], pkt )

    return pkt
