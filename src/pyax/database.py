#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2009  Max Sikström - SA6BBC
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sqlite3

class PktDb:
    def __init__( self, filename ):
        self.conn = sqlite3.connect( filename )
        self.build_tables()



    def build_tables( self ):
        c = self.conn.cursor()
        c.execute("""CREATE TABLE IF NOT EXISTS packets (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            tstamp TEXT(32) DEFAULT CURRENT_TIMESTAMP,
            src TEXT(16),
            dst TEXT(16),
            path TEXT(255) DEFAULT '',
            info BLOB(255)
        )""")
        c.execute("""
            CREATE INDEX IF NOT EXISTS packets_src_dst_info_tstamp
            ON packets (
                src,
                dst,
                info,
                tstamp
            )""")
        c.close()

    def add_packet( self, pkt ):
        c = self.conn.cursor()
        src = pkt.src.plain_encode()
        dst = pkt.dst.plain_encode()
        path = ""
        for node in pkt.path:
            path += "," + node.plain_encode()

        c.execute('INSERT INTO packets (src,dst,path,info) VALUES (?,?,?,?)',
            ( src, dst, path[1:], pkt.info.ax25_encode() )
            )

        c.close()

        self.conn.commit()

    def avalible_since( self, pkt, timemodifier ):
        src = pkt.src.plain_encode()
        dst = pkt.dst.plain_encode()
        info = pkt.info.ax25_encode()

        c = self.conn.cursor()
        c.execute("""
            SELECT COUNT(*) FROM packets
            WHERE src=? AND dst=? AND info=? AND tstamp > datetime('now', ?)
            """, (src, dst, info, timemodifier)
            )

        (count,) = c.fetchone()
        c.close()

        return count > 0
