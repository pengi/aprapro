#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2009  Max Sikström - SA6BBC
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

def decode_mice( info ):
    if info.thirdparty:
        dst = info.thirdparty.dst
    dst = info.dst

    lat = 0
    cls = []

    for c in (dst.name+"      ")[:6]:
        i = ord( c )
        if   '0'<=c and c<='9':
            i-=ord('0')
            cls.append(0)
        elif 'A'<=c and c<='J':
            i-=ord('A')
            cls.append(2)
        elif 'P'<=c and c<='Y':
            i-=ord('P')
            cls.append(1)
        elif c=='K':
            i=0
            cls.append(2)
        elif c=='L':
            i=0
            cls.append(0)
        elif c=='Z':
            i=0
            cls.append(1)
        else:
            assert False # FIXME: throw without assertion

        lat = lat*10 + i

    lat = int(lat/10000) + (lat%10000)/6000.0
    assert lat%10000 < 6000

    assert cls[3] == 0 or cls[3] == 1
    if cls[3] == 0: # North/South
        lat = -lat


    msgabc = cls[0]*9 + cls[1]*6 + cls[2]

    long = ord( info.info[0] )-28
    assert long >= 0
    
    assert cls[4] == 0 or cls[4] == 1
    if cls[4] == 1:
        long += 100

    if 180 <= long and long < 190:
        long -= 80
    if 190 <= long and long < 99:
        long -= 190

    longmin = ord( info.info[1] )-28

    assert longmin >= 0 and longmin < 70

    if longmin>=60:
        longmin -= 60

    longmindec = ord( info.info[2] )-28
    assert longmindec >= 0

    longmin += longmindec/100.0
    long += longmin/60.0

    assert cls[5] == 0 or cls[5] == 1
    if cls[5] == 1:
        long = -long

    speed = ord( info.info[3] )-28
    assert speed >= 0

    speed *= 10

    course = ord( info.info[4] )-28
    assert course >= 0

    speed += course // 10

    course %= 10
    course *= 100

    courseunit = ord( info.info[5] )-28
    assert courseunit >= 0

    course += courseunit

    if speed >= 800:
        speed -= 800
    if course >= 400:
        course -= 400

    assert course <= 360

    sym = info.info[6:8]

    info.params['position'] = (lat, long)
    if course > 0: # course == 0 means no coarse specified, 360 == 0
        info.params['course'] = course%360
    info.params['speed'] = speed
    info.params['symbol'] = sym
    info.params['type'] = 'MicE'

