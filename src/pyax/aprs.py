#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2009  Max Sikström - SA6BBC
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from packet import *
from thirdparty import *
from mice import *

class AprsInfo:
    def __init__( self, info=None, pkt=None ):
        self.rawinfo = info
        self.info = info
        self.thirdparty = None
        self.type = None
        self.params = {}
        if pkt: # Never store pkt, just dst. Avoid cycle references
            self.dst = pkt.dst
        else:
            self.dst = None
        if info != None:
            self.decode()

    def __str__( self ):
        s = ""
        if self.type != None:
            s += self.type + " "
        if self.thirdparty != None:
            s += str( self.thirdparty )
        s += repr( self.params ) + " " + repr( self.info )
        return s

    def __repr__( self ):
        return repr( self.getInfo() )

    def clone( self ): # FIXME: May contain bugs...
        info = AprsInfo()
        info.rawinfo = self.rawinfo
        info.info = self.rawinfo
        info.dst = self.dst
        if info.info != None:
            info.decode()
        return info

    def ax25_encode( self ):
        if self.rawinfo != None:
            return self.rawinfo

        (lat,long) = self.params['position']
        symbol = self.params['symbol']

        lat_deg = int(lat)
        lat_min = (lat%1) * 60.0
        lat_min_h = int(lat_min*100.0)%100
        lat_min = int( lat_min )

        long_deg = int(long)
        long_min = (long%1) * 60.0
        long_min_h = int(long_min*100.0)%100
        long_min = int( long_min )
        return ("!%02d%02d.%02dN" % (lat_deg, lat_min, lat_min_h)) \
                + ("%s%03d%02d.%02dE" % (symbol[0], long_deg, long_min, long_min_h)) \
                + ("%s" % symbol[1]) \
                + self.info


    def decode( self ):
        assert len( self.info ) >= 1

        head = self.info[0]

        if head == '}':
            self.info = self.info[1:]
            self.decode_thirdparty()
            head = self.info[0]

        if head == ';':
            self.type = 'object'
            self.info = self.info[1:]
            self.decode_object()

        if head == ')':
            self.type = 'item'
            self.info = self.info[1:]
            self.decode_item()

        if head == '>':
            self.type = 'status'
            self.info = self.info[1:]
            self.decode_status()

        if head == '\'' or head == '`':
            self.type = 'position'
            self.info = self.info[1:]
            decode_mice( self )

        if head == '=' or head == '!':
            self.type = 'position'
            self.info = self.info[1:]
            self.decode_coord()

        if head == '@' or head == '/':
            self.type = 'position'
            self.info = self.info[1:]
            self.decode_time()
            self.decode_coord()

        if head == '@' or head == '=':
            pass # APRS messaging

    def decode_thirdparty( self ):
        idx = self.info.find(':')
        if idx < 0:
            self.thirdparty = ThirdPartyTag( self.info )
            self.info = ""
        else:
            self.thirdparty = ThirdPartyTag( self.info[:idx] )
            self.info = self.info[(idx+1):]

    def decode_coord( self ):
        assert self.info != ""
        if self.info[0].isdigit():
            # Long format
            assert len( self.info ) >= 19

            sym_tab = self.info[8]
            sym_code = self.info[18]

            lat_deg = int( self.info[0:2] )
            lat_min = float( self.info[2:7] ) # FIXME: ambiguity
            lat_dir = self.info[7]

            long_deg = int( self.info[9:12] )
            long_min = float( self.info[12:17] )
            long_dir = self.info[17]

            lat  = lat_deg  + float(lat_min )/60.0
            assert lat_dir == 'N' or lat_dir == 'S'
            if lat_dir == 'S':
                lat = -lat

            long = long_deg + float(long_min)/60.0
            assert long_dir == 'E' or long_dir == 'W'
            if long_dir == 'W':
                long = -long

            self.info = self.info[19:]
            self.params['type'] = 'plain'

        else:
            # Compressed
            assert len( self.info ) >= 13

            sym_tab = self.info[0]
            sym_code = self.info[9]

            lat_raw = base91_decode( self.info[1:5] )
            lat = 90 - float(lat_raw)/380926.0
            long_raw = base91_decode( self.info[5:9] )
            long = float(long_raw)/190463.0 - 180

            c = base91_decode( self.info[10] )
            s = base91_decode( self.info[11] )

            if c >= 0:
                tflags = base91_decode( self.info[12] )

                if tflags & 0x18 == 0x10:
                    # Altitude

                    self.params['altitude'] = pow( 1.002, c*91+s )
                else:
                    if 0 <= c and c <=89: # Course/Speed:
                        self.params['course'] = c*4
                        self.params['speed'] = pow( 1.08, s ) - 1.0

                    if c == 90: # Precalculated Radio Range
                        self.params['range'] = s * pow( 1.09, s )

            self.info = self.info[13:]
            self.params['type'] = 'compressed'

        self.params['position'] = (lat, long)
        self.params['symbol'] = sym_tab + sym_code


    def decode_time( self ):
        assert len( self.info ) >= 7
        h = int( self.info[0:2] )
        m = int( self.info[2:4] )
        s = int( self.info[4:6] )
        zone = self.info[6]

        self.params['time'] = (h,m,s,zone)

        self.info = self.info[7:]

    def decode_object( self ):
        assert len( self.info ) >= 10
        name = self.info[:9].rstrip()
        life = self.info[9]
        self.info = self.info[10:]

        self.decode_time()
        self.decode_coord()

    def decode_item( self ):
        assert len( self.info ) >= 1
        name = ""
        while self.info[0] != '_' and self.info[0] != '!':
            name += self.info[0]
            self.info = self.info[1:]
            assert len( self.info ) >= 1
        life = self.info[0]
        self.info = self.info[1:]

        self.decode_coord()

    def decode_status( self ):
        assert len( self.info ) >= 7
        if self.info[0:6].isdigit() and self.info[6]=='z':
            self.decode_time()

def base91_decode( str ):
    out = 0
    for cstr in str:
        cid = ord(cstr)-33
        assert cid >= 0 and cid < 91
        out *= 91
        out += cid-33
    return out


def aprsify( pkt ):
    try:
        info = AprsInfo( pkt.info.ax25_encode(), pkt )
        pkt.info = info
    except (ValueError, AssertionError), e:
        print e
